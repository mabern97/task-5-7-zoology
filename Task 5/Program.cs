﻿using System;
using System.Collections.Generic;

namespace Zoology
{
    class Program
    {
        static void Main(string[] args)
        {
            Animal frogAnimal = new Animal("Eddy", "Frog", 3);
            Animal chameleonAnimal = new Animal("Frank", "Chameleon", 10, Gender.Male);

            Animal dogAnimal = new Animal();
            dogAnimal.Name = "Lucy";
            dogAnimal.Species = "Dog";
            dogAnimal.Age = 4;
            dogAnimal.Gender = Gender.Female;

            List<Animal> pets = new List<Animal>();
            pets.Add(frogAnimal);
            pets.Add(chameleonAnimal);
            pets.Add(dogAnimal);

            foreach(Animal pet in pets)
            {
                pet.PrintDescription();
                pet.Walk();
                pet.Jump();
                pet.Sleep();
            }
        }
    }
}
