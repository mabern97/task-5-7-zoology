﻿using System;
namespace Task7Zoology
{
    public enum Gender
    {
        Female,
        Male
    }

    public abstract class Animal
    {
        public string Name{ get; set; }
        public string Species { get; set; }
        public int Age { get; set; }
        public Gender Gender { get; set; }

        public Animal()
        {

        }

        public Animal(string name, string species, int age)
        {
            Name = name;
            Species = species;
            Age = age;
            Gender = Gender.Male;
        }

        public Animal(string name, string species, int age, Gender gender = Gender.Male)
        {
            Name = name;
            Species = species;
            Age = age;
            Gender = gender;
        }

        public virtual void Kill(Animal animal)
        {
            Console.WriteLine($"{Name} found and killed a {animal.Species} named {animal.Name}. :(");
        }

        public virtual void Yawn()
        {
            Console.WriteLine($"{Species}::{Name} yawned. Maybe a bit tired ayy?");
        }

        public virtual void PrintDescription()
        {
            Console.WriteLine($"{Name} is a {Gender.ToString().ToLower()} {Species} who is {Age} years old.");
        }
    }
}
