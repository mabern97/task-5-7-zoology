﻿using System;
namespace Zoology
{
    public enum Gender
    {
        Female,
        Male
    }

    public class Animal
    {
        private string name;

        public string Name{ get => name; set => name = value; }
        public string Species { get; set; }
        public int Age { get; set; }
        public Gender Gender { get; set; }

        public Animal()
        {

        }

        public Animal(string name, string species, int age)
        {
            Name = name;
            Species = species;
            Age = age;
            Gender = Gender.Male;
        }

        public Animal(string name, string species, int age, Gender gender)
        {
            Name = name;
            Species = species;
            Age = age;
            Gender = gender;
        }

        public void PrintDescription()
        {
            Console.WriteLine($"{Name} is a {Gender.ToString().ToLower()} {Species} who is {Age} years old.");
        }

        public void Walk()
        {
            Console.WriteLine($"{Name} the {Species} went on a little walk. Yay?");
        }

        public void Jump()
        {
            Console.WriteLine($"{Name} the {Species} has started jumping all over the place!");
        }

        public void Sleep()
        {
            Console.WriteLine($"{Name} the {Species} decided to take a nap. Zzz");
        }
    }
}
