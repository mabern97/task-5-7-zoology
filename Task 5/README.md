Type: Console Application

Allows a user to manage a group of animals.

Requirements:

•Include an Animal class with
    •Min 2 properties
    •Min 1 behaviour
    •An overloaded constructor
•Create multiple Animal objects and assign the appropriate properties to represent at least three different animals
•Store them in a Collection of your choice
•Display information about these objects from the collection

Purpose: Learn C#: Classes and Objects

Weight: None