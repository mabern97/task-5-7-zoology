﻿using System;
using System.Collections.Generic;

namespace Task7Zoology
{
    class Program
    {
        static void Main(string[] args)
        {
            Cat stella = new Cat("Stella", 8, Gender.Female);
            Cat franky = new Cat("Franky", 4, Gender.Male);

            Dog whiskey = new Dog("Whiskey", 12, Gender.Male);
            Dog lucy = new Dog("Lucy", 4, Gender.Female);

            Tiger vanessa = new Tiger("Vanessa", 10, Gender.Female, "Bengal");
            Lion grant = new Lion("Grant", 6, Gender.Male, "Southwest African");

            List<Animal> pets = new List<Animal>()
            {
                stella,
                franky,
                whiskey,
                lucy,
                vanessa,
                grant
            };

            Cat edward = vanessa.Crossbreed(grant, "Edward");
            Cat brandy = grant.Crossbreed(vanessa, "Brandy");
            franky.Kill(whiskey);

            pets.Add(edward);
            pets.Add(brandy);

            Animal lastAnimal = null;
            foreach (Animal pet in pets)
            {
                pet.PrintDescription();
                pet.Yawn();

                if (lastAnimal != null)
                    lastAnimal.Kill(pet);

                lastAnimal = pet;
            }
        }
    }
}
