﻿using System;
namespace Task7Zoology
{
    public class Cat : Animal
    {
        public bool Striped { get; set; }
        public string Breed { get; set; }

        public Cat(string name, int age, Gender gender)
            :base(name, "Cat", age, gender)
        {
            Striped = false;
            Breed = "Domesticated House Cat";
        }

        public override void PrintDescription()
        {
            Console.WriteLine($"{Name} is a {Age} year old {(Striped == true ? "striped " : "")}{Species} (Breed: {Breed}). Rawr!");
        }

        public override void Kill(Animal animal)
        {
            Console.WriteLine($"{Name} pounced towards a {animal.Species} named {animal.Name}, dinner has been claimed!");
        }

        public Cat Crossbreed(Cat cat, string name)
        {
            string breed = $"{Species[0]}{cat.Species.Substring(1)}";

            Gender gender = this.Gender == Gender.Female && cat.Gender == Gender.Male ? Gender.Male : Gender.Female;

            Cat baby = new Cat(name, 1, gender);
            baby.Breed = $"{this.Gender} {this.Species} {gender} {cat.Species}";

            Console.WriteLine($"Created a {gender} {Species} {cat.Species} cross breed ({breed}), their name is '{name}'. How cute?");

            return baby;
        }
    }

    public class Tiger : Cat
    {

        public Tiger(string Name, int Age, Gender Gender, string breed)
            : base(Name, Age, Gender)
        {
            Species = "Tiger";
            Breed = breed;
        }
    }

    public class Lion : Cat
    {

        public Lion(string Name, int Age, Gender Gender, string breed)
            : base(Name, Age, Gender)
        {
            Species = "Lion";
            Breed = breed;
        }
    }

    public class Liger : Tiger
    {
        public Liger(string name, int age, Gender gender)
            : base(name, age, gender, "Male Lion Female Tiger")
        {

        }
    }

    public class Tigon : Tiger
    {
        public Tigon(string name, int age, Gender gender)
            : base(name, age, gender, "Male Tiger Female Lion")
        {

        }
    }
}
