﻿using System;
namespace Task7Zoology
{
    public class Dog : Animal
    {
        bool Striped { get; set; }
        string Breed { get; set; }

        public Dog(string name, int age, Gender gender)
            : base(name, "Dog", age, gender)
        {

        }

        /*public override void PrintDescription()
        {
            Console.WriteLine($"{Name} is a {(Striped == true ? "striped " : "")} {Species} that is {Age} years old. Rawr!");
        }*/

        public void Pounce(Animal animal)
        {
            Console.WriteLine($"{Name} pounced towards a {animal.Species} named {animal.Name}, dinner has been claimed!");
        }
    }

    public class Greyhound : Dog
    {

        public Greyhound(string Name, int Age, Gender Gender)
            : base(Name, Age, Gender)
        {

        }
    }
}
